ARG IMAGE_NAME
FROM ${IMAGE_NAME}
LABEL maintainer="Daniel von Essen"
ENV container=docker

RUN apk update && \
  apk add python3 sudo

# Create `ansible` user with sudo permissions
RUN addgroup ansible \
  && adduser -D -G ansible ansible \
  && echo "%ansible ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansible
